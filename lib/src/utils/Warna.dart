import 'package:flutter/material.dart';

class Warna {
  static primer() => Colors.blue;
  static sekunder() => Color(0xfff00BCD4);
  static biruLaut() => Color(0xfff006064);
  static sma() => Color(0xfff00695C);
  static umum() => Color(0xfff607D8B);
  static smp() => Color(0xfff0D47A1);

  static cardGradientOne() => Color(0xfff64b3f4);
  static cardGradientTwo() => Color(0xfffc2e59c);
}
