import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

Future showToast(String msg, BuildContext context,
    {int duration, int gravity}) async {
  Toast.show(msg, context, duration: duration, gravity: gravity);
}
