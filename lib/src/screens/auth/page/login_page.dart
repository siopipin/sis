import 'package:flutter/material.dart';
import 'package:sismobile/src/screens/beranda/beranda_screen.dart';
import 'package:sismobile/src/screens/beranda/home_screen.dart';
import 'package:sismobile/src/utils/Warna.dart';
import 'package:sismobile/src/widget/template_curve.dart';
import 'package:sismobile/src/widget/toas.dart';
import 'package:toast/toast.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController cEmail = TextEditingController();
  TextEditingController cPassword = TextEditingController();

  @override
  void initState() {
    super.initState();
    cEmail = TextEditingController(text: "budi@sis.com");
    cPassword = TextEditingController(text: "123456");
  }

  @override
  void dispose() {
    super.dispose();
    cEmail.dispose();
    cPassword.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Stack(
        children: <Widget>[
          ClipPath(
              clipper: TemplateCurve(),
              child: ChildCurveContainer.container(context)),
          ListView(
            children: <Widget>[
              Container(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 100),
                  child: Container(
                    padding: EdgeInsets.only(bottom: 15),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: [
                          BoxShadow(color: Colors.grey[350], blurRadius: 20.0)
                        ]),
                    child: Form(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Container(
                                alignment: Alignment.topCenter,
                                child: Image.asset('assets/images/logo.png',
                                    width: 120.0)),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                            child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey.withOpacity(0.2),
                              elevation: 0.0,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: TextFormField(
                                  controller: cEmail,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Email",
                                    icon: Icon(Icons.alternate_email),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                            child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey.withOpacity(0.2),
                              elevation: 0.0,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: TextFormField(
                                  controller: cPassword,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Password",
                                    icon: Icon(Icons.lock_outline),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
                            child: Material(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Warna.sekunder(),
                                elevation: 0.0,
                                child: MaterialButton(
                                  onPressed: () async {
                                    await doLoginButton(
                                        context: context,
                                        email: cEmail.text,
                                        password: cPassword.text);
                                  },
                                  minWidth: MediaQuery.of(context).size.width,
                                  child: Text(
                                    "Login",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20.0),
                                  ),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      )),
    );
  }
}

Future doLoginButton(
    {@required BuildContext context,
    @required String email,
    @required String password}) async {
  showDialog(
    context: context,
    barrierDismissible: false,
    child: Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    ),
  );
  new Future.delayed(Duration(seconds: 1), () async {
    showToast('Login Success', context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    Navigator.of(context).pop();
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => BerandaScreen()));
  });
}
