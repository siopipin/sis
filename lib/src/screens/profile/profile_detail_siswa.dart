import 'package:flutter/material.dart';
import 'package:sismobile/src/screens/notifikasi/notifikasi_page.dart';
import 'package:sismobile/src/utils/Warna.dart';

class ProfileDetailSiswa extends StatefulWidget {
  ProfileDetailSiswa({Key key}) : super(key: key);

  _ProfileDetailSiswaState createState() => _ProfileDetailSiswaState();
}

class _ProfileDetailSiswaState extends State<ProfileDetailSiswa> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("SIS"),
        backgroundColor: Warna.primer(),
        elevation: 0,
        actions: <Widget>[
          GestureDetector(
            child: Icon(
              Icons.notifications,
              size: 25,
            ),
            onTap: () {
              return Navigator.push(context,
                  MaterialPageRoute(builder: (context) => NotifikasiPage()));
            },
          ),
          SizedBox(
            width: 10,
          )
        ],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            header(context),
          ],
        ),
      ),
    );
  }

  Widget header(BuildContext context) {
    final height = MediaQuery.of(context).size.height / 2;
    final width = MediaQuery.of(context).size.width;
    return Container(
      height: height - 20,
      width: width,
      decoration: BoxDecoration(
        color: Warna.primer(),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(16.0),
          bottomRight: Radius.circular(16.0),
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: width * 0.3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Kelas',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        'XII - A',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17.0,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 140,
                        width: 140,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: new NetworkImage(
                                "http://teenlife.blogs.pressdemocrat.com/files/2010/09/codding.jpg"),
                          ),
                        ),
                      ),
                      SizedBox(height: 15.0),
                      Text(
                        'NIS: 012222001154',
                        style: TextStyle(color: Colors.white70),
                      )
                    ],
                  ),
                ),
                Container(
                  width: width * 0.3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Semester',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        'II',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(16.0),
            child: Text(
              "Susi Pujiastuti",
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
