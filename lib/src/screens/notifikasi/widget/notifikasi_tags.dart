import 'package:flutter/material.dart';
import 'package:sismobile/src/utils/Warna.dart';

class NotifikasiTags extends StatelessWidget {
  final String tagname;
  const NotifikasiTags({Key key, this.tagname}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: tagname == 'SMA'
          ? Warna.sma()
          : tagname == 'UMUM' ? Warna.umum() : tagname == 'SMP' ? Warna.smp() : Warna.sekunder(),
      ),
      padding: EdgeInsets.all(3),
      margin: EdgeInsets.only(right: 3),
      child: Text(tagname, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),),
    );
  }
}
