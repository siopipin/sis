import 'package:flutter/material.dart';
import 'package:sismobile/src/screens/notifikasi/widget/notifikasi_tags.dart';
import 'package:sismobile/src/utils/Warna.dart';

class NotifikasiCard extends StatelessWidget {
  final String title;
  final String subtitle;
  final List tag;
  const NotifikasiCard({Key key, this.title, this.subtitle, this.tag})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Container(
        child: ListTile(
          leading: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.event_note,
                color: Colors.white,
                size: 40,
              )
            ],
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    NotifikasiTags(
                      tagname: tag[0],
                    ),
                    NotifikasiTags(
                      tagname: tag[1],
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2 + 20,
                child: Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
            ],
          ),
          subtitle: Container(
            padding: EdgeInsets.only(top: 5, bottom: 5),
            width: MediaQuery.of(context).size.width / 2,
            child: Text(
              subtitle,
              style: TextStyle(color: Colors.white70),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
          ),
          trailing: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
                size: 20,
              )
            ],
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(
                colors: [Warna.primer(), Warna.sekunder()],
                begin: FractionalOffset(0.0, 0.0),
                end: FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp)),
      ),
    );
  }
}
