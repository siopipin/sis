import 'package:flutter/material.dart';
import 'package:sismobile/src/screens/notifikasi/widget/notifikasi_card.dart';
import 'package:sismobile/src/utils/Warna.dart';
import 'package:sismobile/src/widget/template_curve.dart';

class NotifikasiPage extends StatefulWidget {
  NotifikasiPage({Key key}) : super(key: key);

  NotifikasiState createState() => NotifikasiState();
}

class NotifikasiState extends State<NotifikasiPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SIS"),
        backgroundColor: Warna.primer(),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: <Widget>[
            ClipPath(
              clipper: TemplateCurve(),
              child: ChildCurveContainer.container(context),
            ),
            Container(
              padding: EdgeInsets.only(top: 15, left: 20),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.notifications,
                    color: Colors.white,
                  ),
                  Text(
                    "Pemberitahuan",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  )
                ],
              ),
            ),
            Container(
              child: Positioned(
                width: MediaQuery.of(context).size.width,
                top: 45,
                child: Container(
                  child: notificationBody(context),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget notificationBody(BuildContext context) {
    return Scrollbar(
      child: ListView(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          GestureDetector(
            onTap: () {
              notifikasiDetail(context, "Pengumuman Kelas Baru",
                  "Diinformasikan kepada siswa baru untuk segera melihat daftar kelas masing-masing yang telah di informasikan di majala dinding");
            },
            child: NotifikasiCard(
              title: "Pengumuman Kelas Baru",
              subtitle:
                  "Diinformasikan kepada siswa baru untuk segera melihat daftar kelas masing-masing yang telah di informasikan di majala dinding",
              tag: ['SMA', 'UMUM'],
            ),
          ),
          GestureDetector(
            onTap: () {
              notifikasiDetail(context, "Pengumuman Kelas Baru",
                  "Diinformasikan kepada siswa baru untuk segera melihat daftar kelas masing-masing yang telah di informasikan di majala dinding");
            },
            child: NotifikasiCard(
              title: "Pengumuman Kelas Baru",
              subtitle:
                  "Diinformasikan kepada siswa baru untuk segera melihat daftar kelas masing-masing yang telah di informasikan di majala dinding",
              tag: ['SMA', 'UMUM'],
            ),
          ),
          GestureDetector(
            onTap: () {
              notifikasiDetail(context, "Pengumuman Kelas Baru",
                  "Diinformasikan kepada siswa baru untuk segera melihat daftar kelas masing-masing yang telah di informasikan di majala dinding");
            },
            child: NotifikasiCard(
              title: "Pengumuman Kelas Baru",
              subtitle:
                  "Diinformasikan kepada siswa baru untuk segera melihat daftar kelas masing-masing yang telah di informasikan di majala dinding",
              tag: ['SMA', 'UMUM'],
            ),
          ),
          GestureDetector(
            onTap: () {
              notifikasiDetail(context, "Pembayaran Uang Sekolah",
                  "Diinformasikan kepada siswa baru untuk segera melihat daftar kelas masing-masing yang telah di informasikan di majala dinding");
            },
            child: NotifikasiCard(
              title: "Pembayaran Uang Sekolah",
              subtitle:
                  "Diinformasikan kepada siswa baru untuk segera melihat daftar kelas masing-masing yang telah di informasikan di majala dinding",
              tag: ['SMA', 'UMUM'],
            ),
          ),
        ],
      ),
    );
  }

  Widget notifikasiDetail(BuildContext context, String title, String detail) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        context: context,
        builder: (BuildContext context) {
          return BottomSheet(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            onClosing: () {},
            builder: (BuildContext context) {
              return StatefulBuilder(
                builder: (BuildContext context, setState) {
                  return Material(
                      clipBehavior: Clip.antiAlias,
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.only(
                              topLeft: new Radius.circular(15.0),
                              topRight: new Radius.circular(15.0))),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            headerBottomSheet(context, title),
                            detailNotifikasi(context, detail)
                          ]));
                },
              );
            },
          );
        });
  }

  Widget headerBottomSheet(BuildContext context, String title) => Ink(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Warna.primer(),
            Warna.sekunder(),
          ],
        )),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Icon(
                  Icons.title,
                  color: Colors.white,
                  size: 50,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: Text(
                    title,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget detailNotifikasi(BuildContext context, String detail) {
    return Expanded(
        child: Scrollbar(
            child: ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20, top: 10, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Detail",
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w700,
                              fontSize: 15),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 7,
                    ),
                    Container(
                        child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.timelapse,
                          color: Warna.primer(),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          width: MediaQuery.of(context).size.width - 100,
                          child: Text(
                            detail,
                            textAlign: TextAlign.justify,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 5,
                          ),
                        )
                      ],
                    ))
                  ],
                ),
              ),
              SizedBox(
                height: 12,
              ),
              Divider(),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Diketahui Oleh",
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w700,
                              fontSize: 15),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 7,
                    ),
                    Container(
                        child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.blur_linear,
                          color: Warna.primer(),
                        ),
                        Text('Wali Kelas')
                      ],
                    ))
                  ],
                ),
              ),
              SizedBox(
                height: 12,
              ),
            ],
          ),
        )
      ],
    )));
  }
}
