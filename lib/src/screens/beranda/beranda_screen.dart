import 'package:flutter/material.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:sismobile/src/screens/beranda/home_screen.dart';
import 'package:sismobile/src/screens/berita/berita_screen.dart';
import 'package:sismobile/src/screens/notifikasi/notifikasi_page.dart';
import 'package:sismobile/src/screens/notifikasi/notifikasi_screen.dart';
import 'package:sismobile/src/screens/profile/profile_screen.dart';
import 'package:sismobile/src/utils/Warna.dart';

class BerandaScreen extends StatefulWidget {
  BerandaScreen({Key key}) : super(key: key);

  _BerandaScreenState createState() => _BerandaScreenState();
}

class _BerandaScreenState extends State<BerandaScreen> {
  int currentIndex = 0;
  final List<Widget> _bodyChildren = [
    HomeScreen(),
    // BeritaScreen(),
    // NotifikasiScreen(),
    ProfileScreen()
  ];

  void onTabTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          leading: Icon(Icons.school),
          title: new Text("SIS"),
          backgroundColor: Warna.primer(),
          elevation: 0,
          actions: <Widget>[
            new Stack(children: <Widget>[
              new IconButton(
                  icon: Icon(Icons.notifications),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NotifikasiPage()));
                  }),
              new Positioned(
                right: 11,
                top: 11,
                child: new Container(
                  padding: EdgeInsets.all(2),
                  decoration: new BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 14,
                    minHeight: 14,
                  ),
                  child: Text(
                    '',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 8,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            ])
          ]),
      body: _bodyChildren[currentIndex],
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: currentIndex,
        showElevation: true,
        onItemSelected: (index) => setState(() {
          currentIndex = index;
        }),
        items: [
          BottomNavyBarItem(
            icon: Icon(Icons.apps),
            title: Text('Beranda'),
            activeColor: Warna.primer(),
          ),
          // BottomNavyBarItem(
          //     icon: Icon(Icons.library_books),
          //     title: Text('Berita'),
          //     activeColor: Warna.primer()),
          // BottomNavyBarItem(
          //     icon: Icon(Icons.notification_important),
          //     title: Text('Notifikasi'),
          //     activeColor: Warna.primer()),
          BottomNavyBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile'),
              activeColor: Warna.primer()),
        ],
      ),
    );
  }
}
