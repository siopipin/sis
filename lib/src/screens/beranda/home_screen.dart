import 'package:flutter/material.dart';
import 'package:sismobile/src/screens/profile/profile_detail_siswa.dart';
import 'package:sismobile/src/utils/Warna.dart';
import 'package:sismobile/src/widget/template_curve.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: <Widget>[
          widgetSambutan(context),
          widgetDaftarAnak(context),
        ],
      ),
    );
  }

  Widget widgetSambutan(BuildContext context) {
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [0, 1],
              colors: [Warna.primer(), Warna.sekunder()])),
      child: Container(
        padding: EdgeInsets.only(top: 15, left: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Selamat Datang",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            Text("Sio Jurnalis Pipin",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 22))
          ],
        ),
      ),
    );
  }

  Widget widgetDaftarAnak(BuildContext context) {
    return Positioned(
      top: 90,
      child: Container(
        height: 100,
        width: MediaQuery.of(context).size.width,
        child: PageView(
          controller: PageController(
            initialPage: 1,
            viewportFraction: 0.8,
          ),
          children: [
            widgetCard(context),
            widgetCard(context),
            widgetCard(context),
          ],
        ),
      ),
    );
  }

  Widget widgetCard(BuildContext context) {
    return Stack(
      children: <Widget>[
        GestureDetector(
          child: Container(
            child: Card(
              child: Container(
                decoration: widgetDecorationCard(),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 90,
                      child: Container(
                        child: Image(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'http://teenlife.blogs.pressdemocrat.com/files/2010/09/codding.jpg'),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                          padding: EdgeInsets.only(top: 4, left: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Susi Pujiastuti",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    color: Colors.white),
                              ),
                              Text("XII - A",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                      color: Colors.white)),
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Warna.sma()),
                                    padding: EdgeInsets.all(3),
                                    margin: EdgeInsets.only(right: 3),
                                    child: Text(
                                      "SMA",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Warna.umum()),
                                    padding: EdgeInsets.all(3),
                                    margin: EdgeInsets.only(right: 3),
                                    child: Text(
                                      "UMUM",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                padding: EdgeInsets.only(right: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text("Lihat Detail",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12)),
                                    Icon(
                                      Icons.arrow_forward,
                                      color: Colors.white,
                                      size: 12,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )),
                    )
                  ],
                ),
              ),
            ),
          ),
          onTap: () {
            return Navigator.push(context,
                MaterialPageRoute(builder: (context) => ProfileDetailSiswa()));
          },
        )
      ],
    );
  }
}

widgetDecoration() {
  return BoxDecoration(
      gradient: new LinearGradient(
          colors: [Warna.primer(), Warna.sekunder()],
          begin: FractionalOffset(0, 0),
          end: FractionalOffset(1, 0),
          stops: [0, 1],
          tileMode: TileMode.clamp));
}

widgetDecorationCard() {
  return BoxDecoration(
      gradient: new LinearGradient(
          colors: [
            Warna.cardGradientOne(),
            Warna.cardGradientTwo(),
          ],
          begin: FractionalOffset(0, 0),
          end: FractionalOffset(1, 0),
          stops: [0, 1],
          tileMode: TileMode.clamp));
}
